﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GlobalGameEvents
{
    /// <summary>
    /// <para>Abstract base class for all global game events. It puts typical C# events into a scriptable object wrapper to work nicely with Unity's workflow.</para>
    /// <para></para> 
    /// <para> Inherit from this class to add functionality.</para>
    /// </summary>
    /// <typeparam name="T">Where T : GlobalGameEventInfo</typeparam>
    public abstract class GlobalGameEvent<T> : ScriptableObject where T : GlobalGameEventInfo
    {
        protected const bool DEBUG_OUTPUT = true;

        public delegate void EventInvoke(T info);
        /// <summary>
        /// The C# event. Register Listeners to this event.
        /// </summary>
        public event EventInvoke m_event;

        /// <summary>
        /// Call this when you want to trigger the event. All events expect a GlobalGameEventInfo passed in with it.
        /// </summary>
        /// <param name="info">The GlobalGameEventInfo passed with the event. Carries information like source and target.</param>
        public virtual void Raise(T info)
        {
            if (DEBUG_OUTPUT)
            {
                string sourceName = info.m_sourceGameObject == null ? "NULL" : info.m_sourceGameObject.name;
                string targetName = info.m_targetGameObject == null ? "NULL" : info.m_targetGameObject.name;
                string output = "EVENT " + this.name + ": " + sourceName + " to " + targetName + " for " + info.m_amount;
                Debug.Log(output);
            }
            if (m_event != null) m_event(info);

        }
    }

}



