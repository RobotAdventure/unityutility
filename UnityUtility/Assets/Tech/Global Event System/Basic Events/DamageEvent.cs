﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalGameEvents;

namespace GlobalGameEvents
{
    [CreateAssetMenu(menuName = "GlobalGameEvent/DamageEvent", fileName = "Damage Event")]
    public class DamageEvent : GlobalGameEvent<DamageEventInfo>
    { }
}

