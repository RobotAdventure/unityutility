﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalGameEvents;

namespace GlobalGameEvents
{
    /// <summary>
    /// A basic GGE inherited from the abstract class.
    /// </summary>
    [CreateAssetMenu(menuName = "GlobalGameEvent/BasicEvent", fileName = "GlobalGameEvent")]
    public class BasicGlobalGameEvent : GlobalGameEvent<GlobalGameEventInfo>
    { }
}

