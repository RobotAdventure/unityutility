﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GlobalGameEvents
{
    /// <summary>
    /// Information that is passed with each event.
    /// </summary>
    public class GlobalGameEventInfo
    {
        /// <summary>
        /// The source of the event. May not be who called the event.
        /// </summary>
        public Object m_sourceGameObject;

        /// <summary>
        /// The target of the event. Use this to filter against so a target knows if it's been called.
        /// </summary>
        public Object m_targetGameObject;

        /// <summary>
        /// An amount that might be relevant to the event such as how much damage is delt.
        /// </summary>
        public float m_amount;

        /// <summary>
        /// The constructor. Only requires a source.
        /// </summary>
        /// <param name="source">The object that triggered the event.</param>
        /// <param name="target">The object the event is targeting.</param>
        /// <param name="amount">An amount relevant to the event</param>
        public GlobalGameEventInfo(Object source, Object target = null, float amount = 0)
        {
            m_sourceGameObject = source;
            if (target != null) m_targetGameObject = target;
            m_amount = amount;
        }

        public bool CheckInfoType<T>(out T infoAsType) where T : GlobalGameEventInfo
        {
            if (this is T)
            {
                infoAsType = (T)this;
                return true;
            }
            else
            {
                infoAsType = null;
                return false;
            }
        }
    }
}

