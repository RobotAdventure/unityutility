﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalGameEvents;

public class GGETestReciever : MonoBehaviour
{
    public BasicGlobalGameEvent m_clickEvent;

    public void OnEnable()
    {
        m_clickEvent.m_event += RespondToClick;
    }

    public void OnDisable()
    {
        m_clickEvent.m_event -= RespondToClick;
    }

    public void RespondToClick(GlobalGameEventInfo info)
    {
        Debug.Log("CLICK EVENT FROM: " + info.m_sourceGameObject.name + " TO " + gameObject.name);
    }
}
