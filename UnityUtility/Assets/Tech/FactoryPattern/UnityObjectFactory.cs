﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Factories;

namespace Factories
{
    //Unity Object Factory is a generic factory to get a references of various assets.
    //Does not create objects. If you want to instantiate game objects, use GameObjectFactory instead
    [CreateAssetMenu(menuName = "Factory/Unity_Object_Factory", fileName = "Unity Object Factory")]
    public class UnityObjectFactory : Factory<Object, UnityObjectSpawnStats>
    {
            
    }
}
