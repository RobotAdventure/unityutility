﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;



namespace Factories
{
    //The factory class is a scriptable object with a list of spawn stats.
    //The Spawn stats contain a weight. The factory, based off of the total weight, finds a random spawn stat
    //Spawn then returns the variable of type "TReturnType" from the spawnstat
    
    public abstract class Factory<TReturnType, TSpawnStats> : ScriptableObject where TSpawnStats : SpawnStats<TReturnType>  //inherit from this to create
    {
        [SerializeField]
        private TSpawnStats[] m_spawnsStats;

        /// <summary>
        /// Just returns a reference from the selected spawnstats
        /// </summary>
        /// <returns>Reference to the object</returns>
        public virtual TReturnType GetSpawn() 
        {
            return GetRandomSpawn().m_spawnTarget;
        }

        /// <summary>
        /// //Creates a new instance of the spawn target from the selected spawn.
        /// </summary>
        /// <returns>An instance of the selected spawn</returns>
        public virtual TReturnType CreateSpawn()  
        {
            Debug.LogWarning("You are attempting to create an object in a factory where the 'CreateSpawn' Method has not been overriden. The base method only returns a reference.");
            return GetSpawn();
        }

        TSpawnStats GetRandomSpawn()
        {
            Assert.AreNotEqual(m_spawnsStats.Length, 0, "m_spawnStats is empty. The Array must be populated for the factory to work");
            TSpawnStats returnSpawnStat = m_spawnsStats[0];
            float totalWeight = 0.0f;

            foreach (TSpawnStats statWeight in m_spawnsStats) totalWeight += statWeight.m_weight;

            float randomFloat = Random.Range(0, totalWeight);
            totalWeight = 0.0f;
            foreach (TSpawnStats stat in m_spawnsStats)
            {
                totalWeight += stat.m_weight;
                if (totalWeight > randomFloat)
                {
                    returnSpawnStat = stat;
                    break;
                }
            }
            return returnSpawnStat;
        }
    }

    public class SpawnStatsBase
    {
        public string name;
        public float m_weight;
    }

    public class SpawnStats<T> : SpawnStatsBase
    {
        public T m_spawnTarget;
    }

    [System.Serializable]
    public class UnityObjectSpawnStats : SpawnStats<Object> { }

    [System.Serializable]
    public class GameObjectSpawnStats : SpawnStats<GameObject> { }

}
