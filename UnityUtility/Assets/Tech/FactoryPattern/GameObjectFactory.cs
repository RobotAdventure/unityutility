﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Factories;

namespace Factories
{
    [CreateAssetMenu(menuName = "Factory/Game_Object_Factory", fileName = "Game Object Factory")]
    public class GameObjectFactory : Factory<GameObject, GameObjectSpawnStats>
    {
        public override GameObject CreateSpawn()
        {
            return Instantiate(GetSpawn());
        }
    }
}
