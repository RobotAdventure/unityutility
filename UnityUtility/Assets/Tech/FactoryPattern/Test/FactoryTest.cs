﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactoryTest : MonoBehaviour
{

    public Factories.GameObjectFactory m_factory;

    void OnGUI()
    {
        if (GUI.Button(new Rect(Screen.width / 2 - 50, 5, 100, 30), "Get Reference"))
        {
            Debug.Log("Obtained Reference to " + m_factory.GetSpawn().ToString() + " From Factory");
        }

        if (GUI.Button(new Rect(Screen.width / 2 - 50, 40, 100, 30), "Create Spawn Target"))
        {
            Debug.Log("Creating  " + m_factory.CreateSpawn().ToString() + " From Factory");
        }
    }
}
