﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariableSetTest : MonoBehaviour
{
    public int m_counter = 0;
    public GameObjectSet m_pool;

    void OnGUI()
    {
        if (GUI.Button(new Rect(Screen.width / 2 - 50, 5, 100, 30), "Add New GO"))
        {
            AddObject();
        }

        if (GUI.Button(new Rect(Screen.width / 2 - 50, 40, 100, 30), "Lift All In Pool"))
        {
            LiftObjects();
        }
    }

    void AddObject()
    {
        m_pool.Add(new GameObject(m_counter.ToString()));
        m_counter++;
    }

    void LiftObjects()
    {
        foreach(GameObject go in m_pool)
        {
            go.transform.Translate(Vector3.up * .5f);
        }
    }

    private void OnDisable()
    {
        m_pool.m_list.Clear();

    }
}
