﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalGameEvents;

namespace VariableSets
{
    /// <summary>
    /// Variable Pool base class that tracks a list of type T. Inherits from Global Game Event and triggers whenever the list is updated.
    /// </summary>
    /// <typeparam name="T">T is a Unity Object (for now)</typeparam>
    public class VariableSet<T> : BasicGlobalGameEvent, IEnumerable where T : Object
    {
        public List<T> m_list;

        public virtual void Add(T target)
        {
            if(!m_list.Contains(target))
            {
                m_list.Add(target);
                Raise(new GlobalGameEventInfo(this, target));
            }
            
        }

        public virtual void Remove(T target)
        {
            if (m_list.Contains(target))
            {
                m_list.Remove(target);
                Raise(new GlobalGameEventInfo(this, target));
            }
        }

        public List<T> GetNewListOfPool()
        {
            return new List<T>(m_list);
        }

        public T GetRandomFromPool()
        {
            if (m_list.Count == 0) return null;
            else return m_list[Random.Range(0, m_list.Count)];
        }

        //Use the protected list as the enumerator if something needs to go over the entire list
        public IEnumerator GetEnumerator()
        {
            return m_list.GetEnumerator();
        }

        [ContextMenu("OutputSet")]
        public void DebugOutputSet()
        {
            string output = "";
            foreach(T item in m_list)
            {
                output += item.name + "\n";
            }

            Debug.Log(this.name + " List Items: \n" + output);
        }

    }
}