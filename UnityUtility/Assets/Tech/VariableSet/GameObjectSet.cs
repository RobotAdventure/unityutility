﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VariableSets;

[CreateAssetMenuAttribute(menuName = "Variable_Set/Game_Object_Set", fileName = "Game Object Set")]
public class GameObjectSet : VariableSet<GameObject>
{ }
