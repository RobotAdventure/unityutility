﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalGameEvents; 
using ScriptableObjectVariables;

namespace ScriptableObjectVariables
{

    [CreateAssetMenuAttribute(menuName = "Variables/FloatVariable", fileName = "FloatVariable")]
    public class FloatVariable : BasicGlobalGameEvent, ISerializationCallbackReceiver
    {
        public float m_initialValue;

        public virtual float value
        {
            get { return m_runtimeValue; }
            set
            {
                if (value != m_runtimeValue)
                {
                    m_runtimeValue = value;
                    Raise(new GlobalGameEventInfo(this)); //Trigger Event so things no it changed
                }
            }
        }

        [SerializeField]
        protected float m_runtimeValue;

        public virtual void OnAfterDeserialize()
        {
            m_runtimeValue = m_initialValue;
        }

        public virtual void OnBeforeSerialize()
        {

        }

        public void OnDisable()
        {

        }
    }

}
