﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCommand : Command
{
    public Vector3 m_targetPosition;
    public Vector3 m_beforePosition;
    public GameObject m_targetActor;

    public MoveCommand(GameObject actor, Vector3 targetPosition)
    {
        m_targetActor = actor;
        m_beforePosition = m_targetActor.transform.position;
        m_targetPosition = targetPosition;
    }
    public override void Execute()
    {
        m_targetActor.transform.position = m_targetPosition;
    }

    public override void Undo()
    {
        m_targetActor.transform.position = m_beforePosition;
    }

}
