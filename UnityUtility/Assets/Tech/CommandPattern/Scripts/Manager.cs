﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    public List<Command> m_commands = new List<Command>();
    public GameObject m_cube;

    private int m_commandIndex = -2;
	
    // Use this for initialization
	void Start ()
    {
        AddCommand(new MoveCommand(m_cube, m_cube.transform.position));
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow)) AddCommand(new MoveCommand(m_cube, m_cube.transform.position + Vector3.forward));
        if (Input.GetKeyDown(KeyCode.RightArrow)) AddCommand(new MoveCommand(m_cube, m_cube.transform.position + Vector3.right));
        if (Input.GetKeyDown(KeyCode.DownArrow)) AddCommand(new MoveCommand(m_cube, m_cube.transform.position + -Vector3.forward));
        if (Input.GetKeyDown(KeyCode.LeftArrow)) AddCommand(new MoveCommand(m_cube, m_cube.transform.position + -Vector3.right));

        if (Input.GetKeyDown(KeyCode.Z)) UndoCommand();
        if (Input.GetKeyDown(KeyCode.Y)) RedoCommand();
    }

    public void AddCommand(Command command)
    {
        if (m_commandIndex == -2) m_commandIndex = 0; //First part of the list has been added
        else if (m_commandIndex == m_commands.Count - 1) m_commandIndex++; //We are keeping up with the list, add one to the index
        else //We aren't at the top so clear the rest of the list
        {
            m_commands.RemoveRange(m_commandIndex + 1, m_commands.Count - (m_commandIndex + 1));
            m_commandIndex++;
        }

        m_commands.Add(command);
        command.Execute();
    }

    public void UndoCommand()
    {
        if (m_commandIndex > 0)
        {            
            m_commands[m_commandIndex].Undo();
            m_commandIndex--;
        }                
    }

    public void RedoCommand()
    {
        if(m_commandIndex < m_commands.Count - 1 )
        {
            m_commandIndex++;
            m_commands[m_commandIndex].Execute();                        
        }
    }
}
