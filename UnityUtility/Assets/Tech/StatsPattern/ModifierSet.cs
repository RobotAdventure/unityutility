﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ModifierSet : UnitySerializedDictionary<Stat, List<StatModifier>>
{
    public ModifierSet(ModifierSet copyTarget) : base(copyTarget)
    { }

    public ModifierSet()
    { }
}
