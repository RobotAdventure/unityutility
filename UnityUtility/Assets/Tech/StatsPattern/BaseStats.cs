﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// BaseStats are essentially a scriptable object wrapper around a StatSet. Expected to be loaded into an ActiveStats component on Start.
/// </summary>
[CreateAssetMenu(menuName = "Stats/BaseStat", fileName = "NewBaseStat")]
public class BaseStats : ScriptableObject
{
    /// <summary>
    /// The stat type tells the calculation whether to treat the number as an int or a float.
    /// </summary>
    public StatSet m_statSet = new StatSet();
}
