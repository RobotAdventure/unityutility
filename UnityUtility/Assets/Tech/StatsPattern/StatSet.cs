﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A shorthand way to represent a set of stats. Several scripts accross the pattern will use a similar setup so I figured I'd make a class out of it.
/// </summary>

[System.Serializable]
public class StatSet : UnitySerializedDictionary<Stat, float>
{
    public StatSet(StatSet copyTarget) : base(copyTarget)
    { }

    public StatSet()
    { }

    public float GetStat(Stat targetStat)
    {
        if (this.ContainsKey(targetStat))
        {
            return targetStat.GetValue(this[targetStat]);
        }
        else return targetStat.GetValue(0.0f); //Doesn't contain stat so the stat is zero
    }
}
