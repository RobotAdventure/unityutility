﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Courtesy of Odin Inspector's documentation:
//https://odininspector.com/tutorials/serialize-anything/serializing-dictionaries

public abstract class UnitySerializedDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver
{
	[SerializeField]
	private List<TKey> keyData = new List<TKey>();
	
	[SerializeField]
	private List<TValue> valueData = new List<TValue>();

    void ISerializationCallbackReceiver.OnAfterDeserialize()
    {
		this.Clear();
		Debug.Log("Clearing Dictionary");
		for (int i = 0; i < this.keyData.Count && i < this.valueData.Count; i++)
		{
			this[this.keyData[i]] = this.valueData[i];
		}
    }

    void ISerializationCallbackReceiver.OnBeforeSerialize()
    {

		//Debug.Log("Serializing Dictionary");
		this.keyData.Clear();
		this.valueData.Clear();

		foreach (var item in this)
		{
			this.keyData.Add(item.Key);
			this.valueData.Add(item.Value);
		}
    }

	public UnitySerializedDictionary(UnitySerializedDictionary<TKey, TValue> copyTarget) : base(copyTarget)
	{}

	public UnitySerializedDictionary() {}
}