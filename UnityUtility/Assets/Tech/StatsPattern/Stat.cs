﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Represents a stat like a data based enum. Passed around as reference.
/// </summary>
[CreateAssetMenu(menuName = "Stats/NewStat", fileName = "NewStat")]
public class Stat : ScriptableObject
{
    /// <summary>
    /// The name to show in custom interfaces
    /// </summary>
    public string m_displayName;

    /// <summary>
    /// The stat type tells the calculation whether to treat the number as an int or a float.
    /// </summary>
    public StatType m_statType;

    /// <summary>
    /// Whether the stat in question is clamped to a range;
    /// </summary>
    public bool m_limitRange;

    /// <summary>
    /// The min the stat can be.
    /// </summary>
    public float m_statMin;

    /// <summary>
    /// The max the stat can be.
    /// </summary>
    public float m_statMax;

    /// <summary>
    /// Used for displaying stats. Will display stats in groups.
    /// </summary>
    public string m_group;

    /// <summary>
    /// Where the stat is displayed within the same gorup
    /// </summary>
    public int m_groupOrder = 0;

    public virtual float GetValue(float amount)
    {
        float returnedFloat = amount;
        if(m_limitRange)
        {
            returnedFloat = Mathf.Clamp(returnedFloat, m_statMin, m_statMax);
        }

        if(m_statType == StatType.intType) returnedFloat = Mathf.RoundToInt(returnedFloat);

        return returnedFloat;
    }
}

public enum StatType
{
    intType = 0,
    floatType = 1,
}
