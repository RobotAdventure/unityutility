﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Acts as a wrapper to a stat modifier so that it can be used as a data object inside unity.
/// </summary>
[CreateAssetMenu(menuName = "Stats/StatModifier", fileName = "NewStatModifier")]
public class StatModifierSO : ScriptableObject
{
    public StatModifier m_modifier;
}
