﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Active stats are the game object's current stats. This is what all game-time logic will reference.
/// </summary>
public class ActiveStats : MonoBehaviour
{
    [Header("Stats")]
    public StatSet m_activeStats;
    public BaseStats m_baseStats;

    [Header("Stat Modifiers")]
    
    public ModifierSet m_modifiers;

    private void Awake()
    {
        LoadBaseStats(m_baseStats);
    }

    private void LoadBaseStats(BaseStats baseStats)
    {
        m_activeStats = new StatSet(baseStats.m_statSet);
    }

    
    public void AddModifier(StatModifier newModifier)
    {
        if(m_modifiers.ContainsKey(newModifier.m_targetStat))
        {
            m_modifiers[newModifier.m_targetStat].Add(newModifier);
        }
        else
        {
            m_modifiers.Add(newModifier.m_targetStat, new List<StatModifier>());
            m_modifiers[newModifier.m_targetStat].Add(newModifier);
        }
    }

    [Sirenix.OdinInspector.Button]
    public void AddModifier(StatModifierSO newModifierSO)
    {
        AddModifier(new StatModifier(newModifierSO.m_modifier));
    }

    public float GetStat(Stat targetStat)
    {
        float baseValue = m_activeStats.GetStat(targetStat);
        if(m_modifiers.ContainsKey(targetStat))
        {
            foreach(StatModifier mod in m_modifiers[targetStat])
            {
                baseValue = mod.Modify(baseValue);
            }

            baseValue = targetStat.GetValue(baseValue); //Filter the modified value back through the stat to make sure we're still within its constraints.
        }

        return baseValue;
    }

    [Sirenix.OdinInspector.Button]
    public void DebugStat(Stat targetStat)
    {
        Debug.Log(targetStat.name + " value: " + GetStat(targetStat));
    }



}
