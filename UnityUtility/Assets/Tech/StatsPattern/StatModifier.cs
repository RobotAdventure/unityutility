﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The Stat Modifier is a 
/// </summary>
[System.Serializable]
public class StatModifier
{
    public Stat m_targetStat;
    public ModifierType m_modifierType;
    public float m_amount;


    public StatModifier()
    {

    }

    public StatModifier(StatModifier copyTarget)
    {
        m_targetStat = copyTarget.m_targetStat;
        m_modifierType = copyTarget.m_modifierType;
        m_amount = copyTarget.m_amount;
    }

    public float Modify(float baseAmount)
    {
        if (m_modifierType == ModifierType.FLAT_AMOUNT)
        {
            baseAmount += m_amount;
        }
        else if (m_modifierType == ModifierType.PERCENT)
        {
            baseAmount *= m_amount;
        }

        return baseAmount;
    }

}

[System.Serializable]
public enum ModifierType
{
    FLAT_AMOUNT = 0,
    PERCENT
}
